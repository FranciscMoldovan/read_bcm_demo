Project Structure:   

project root -> images/ (directory with the images)  
             -> read_bcm_demo/ (Qt QML Application directory)  
             -> car_and_lights (GIMP project with car w/ lights)   
How to run:  
-> After cloning the project, upon startup in Qt Creator IDE, please create a build/ directory in which to store    
   all the compilation data.  
-> The program should work without problems