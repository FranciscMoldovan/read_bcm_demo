import QtQuick 2.5
import QtQuick.Window 2.2

Window {
    visible: true
    width: 1280
    height: 960
    title: qsTr("read_bcm_demo")

    MainForm {
        anchors.fill: parent
        mouseArea.onClicked: {}
        objectName: "test_name"
    }
}
