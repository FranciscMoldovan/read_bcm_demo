import QtQuick 2.0

Item {
    id: container
    property alias cellColor: rectangle.color
    property alias cellText: cellText.text
    signal clicked(color cellColor)

    width: 90; height: 45
 Rectangle {
        id: rectangle
        border.color: "white"
        anchors.fill: parent

        Text{
            id:cellText
            font.pointSize: 24; font.bold: true
        }
    }
  MouseArea {
        anchors.fill: parent
        onClicked: container.clicked(container.cellColor)
    }
}
