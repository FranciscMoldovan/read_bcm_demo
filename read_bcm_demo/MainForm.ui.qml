import QtQuick 2.5

Rectangle {

    property alias mouseArea: mouseArea
    color: "#377a1f"

    MouseArea {
        id: mouseArea
        width: 808
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent

        Grid {
                id: colorPicker
//                x: 4; anchors.bottom: page.bottom; anchors.bottomMargin: 4
//                rows: 2; columns: 3; spacing: 3

                Cell { cellColor: "red"; onClicked: car.visible = false}
                Cell { cellColor: "green"; onClicked: car.visible = true }
            }

        Image {
            id: car
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/car.png"
        }

        Image {
            id: brakelamp
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/brake_lamp.png"
        }
        Image {
            id: left_back_left_signal
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/left_back_left_signal.png"
        }
        Image {
            id: left_backlight
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/left_backlight.png"
        }
        Image {
            id: left_headlamp
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/left_headlamp.png"
            objectName: "left_light"

        }
        Image {
            id: left_reverse
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/left_reverse.png"
        }
        Image {
            id: left_signal_front
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/left_signal_front.png"
        }
        Image {
            id: right_backlight
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/right_backlight.png"
        }
        Image {
            id: right_headlamp
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/right_headlamp.png"
        }
        Image {
            id: right_reverse
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/right_reverse.png"
        }
        Image {
            id: signal_back_right
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/signal_back_right.png"
        }
        Image {
            id: signal_front_right
            x: 392
            y: 4
            width: 497
            height: 977
            fillMode: Image.PreserveAspectCrop
            source: "../images/signal_front_right.png"
        }
    }
}











